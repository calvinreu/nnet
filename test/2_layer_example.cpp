#include <nnet/neural_network.hpp>
#include <vector>
#include <iostream>

double ReLu(const double &x)
{
    return x;
}

int main()
{
    double input[4] = {5, 4, 3, 10};
    double output[1];
    nnet::neural_network testNet(4, 1, &ReLu);

    //std::cout << testNet.m_hidden[0].m_connections[0].m_input << std::endl;
    //std::cout << &(testNet.m_input[0].m_output) << std::endl;
    //std::cout << testNet.m_hidden[0].m_connections[1].m_input << std::endl;
    //std::cout << &(testNet.m_input[1].m_output) << std::endl;
    //std::cout << testNet.m_output[0].m_connections[0].m_input << std::endl;
    //std::cout << &(testNet.m_hidden[0].m_output) << std::endl;

    testNet.run(output, input);

    std::cout << *output << std::endl;
}
