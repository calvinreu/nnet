#include "neuron.hpp"

void nnet::NEURON::hidden::update()
{
    m_output = 0;

    for (auto i = m_connections.begin(); i < m_connections.end(); i++)
        m_output += i->update();

    m_output = m_akt(m_output);

    //std::cout << m_output << std::endl;

}

void nnet::NEURON::output::update(double &output)
{
    output = 0;

    for (auto i = m_connections.begin(); i < m_connections.end(); i++)
        output += i->update();

    output = m_akt(output);
    
}