# nnet
the idea behind nnet is to be able to build a fully customizable neural network with customizable learning methods.

## required libraries
 - [vector](https://gitlab.com/calvinreu/vector)

## TODO
 - add error handling when allocating memory

## in progress

## License

[![AGPL-3.0 License](https://www.gnu.org/graphics/agplv3-155x51.png)](https://www.gnu.org/licenses/agpl.html)

Libraries
Copyright (C) 2019 Calvin Reu

SPDX-License-Identifier: AGPL-3.0

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see [https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).

## variables

### m_input
the input neurons of the neural network

### m_hidden
the hidden neurons of the neural network

### m_output
the output neurons of the neural network

## member functions

### constructor

create a neural network with a aktivation function for all neurons and connection to every neuron in the next layer.

> neural_network(const size_t &input_neurons, const size_t &hidden_neurons, const size_t &output_neurons, double (*akt)(double&));

### run
run the neural network function with the input and set the output
> void run(double* output_location, double* input_location);

## input functions

### constructor
define the activation function

> input(double (*akt)(double&));

### update
update the neuron output

> void update(const double &input);

## hidden functions

### constructor
define the activation function and allocate memory for the connections

> hidden(const size_t &connectionC, double(*akt)(double&));

### update
update neuron output

> update()

## output functions

### constructor
define the activation function and allocate memory for the connections.

> output(const size_t &connectionC, double (*akt)(double&));

### update
update the output

> void update(double &output);

## Multiple Hidden Layers
Multiple hidden Layers can be created by putting "blocks" of neureons, which are connected to the previous "block".
Note: the layers must be created behind each other. The memory location of layer 1 has to be smaller then the memory location of layer 2.
[Multiple hidden Layers](./Readme_img/wunderfolles model.jpg)