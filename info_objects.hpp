#include <extended/vector>

using extended::vector;

namespace nnet{

    enum layer{
        input  = 0,
        hidden = 1,
        output = 2
    };

    struct connection_info
    {
        nnet::layer startLayer;
        size_t startNeuron;
        double weight;

        connection_info(){}
        connection_info(nnet::layer startLayer, size_t startNeuron, double weight) : startLayer(startLayer), startNeuron(startNeuron), weight(weight){}
    };

    struct hidden_neuron_info
    {
        vector<nnet::connection_info> connections;
        double (*aktivationFunction)(const double&);

        hidden_neuron_info(){}
        hidden_neuron_info(double (*aktivationFunction)(const double&)) : aktivationFunction(aktivationFunction){}
    };

    typedef hidden_neuron_info output_neuron_info;

    struct input_neuron_info
    {
        double (*aktivationFunction)(const double&);
        
        input_neuron_info(){}
        input_neuron_info(double (* const& aktivationFunction)(const double&)) : aktivationFunction(aktivationFunction){}
    };
    
    struct neural_network_info
    {
        vector<input_neuron_info>  inputLayer ;
        vector<hidden_neuron_info> hiddenLayer;
        vector<output_neuron_info> outputLayer;
    
        neural_network_info(){}
    };
    
}