CC = g++ -std=c++17 -g -c -fpic
gccV := $(shell gcc -dumpversion)

default:
	make nnet

nnet: neural_network.hpp neural_network.cpp neuron.hpp neuron.cpp connection.hpp
	$(CC)  neuron.cpp -o neuron.o
	$(CC)  neural_network.cpp -o neural_network.o
	file neuron.o
	file neural_network.o
	gcc -shared neural_network.o neuron.o -o libnnet.so
	rm -f neural_network.o
	rm -f neuron.o

clean:
	rm -r /usr/include/c++/$(gccV)/nnet

install: libnnet.so neural_network.hpp neuron.hpp connection.hpp datatypes.hpp info_objects.hpp functions.hpp
	mv libnnet.so /usr/lib64
	chmod 0755 /usr/lib64/libnnet.so
	mkdir -p /usr/include/c++/$(gccV)/nnet
	cp neural_network.hpp /usr/include/c++/$(gccV)/nnet
	chmod 0755 /usr/include/c++/$(gccV)/nnet neural_network.hpp
	cp neuron.hpp /usr/include/c++/$(gccV)/nnet
	chmod 0755 /usr/include/c++/$(gccV)/nnet neuron.hpp
	cp connection.hpp /usr/include/c++/$(gccV)/nnet
	chmod 0755 /usr/include/c++/$(gccV)/nnet connection.hpp
	cp info_objects.hpp /usr/include/c++/$(gccV)/nnet
	chmod 0755 /usr/include/c++/$(gccV)/nnet info_objects.hpp
	cp datatypes.hpp /usr/include/c++/$(gccV)/nnet
	chmod 0755 /usr/include/c++/$(gccV)/nnet datatypes.hpp
	cp functions.hpp /usr/include/c++/$(gccV)/nnet
	chmod 0755 /usr/include/c++/$(gccV)/nnet functions.hpp