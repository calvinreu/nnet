#include "connection.hpp"
#include <extended/vector>

using extended::vector;

namespace nnet::NEURON{

    struct input
    {
        double m_output;
        double (*m_akt)(const double&);//aktivation function

        input(double (*akt)(const double&))
        : m_akt(akt){}

        input(){}

        void update(const double &input){m_output = m_akt(input);}

    };

    struct hidden{
        
        vector<CONNECTION::connection> m_connections;//the connections ending at this neuron
        double m_output;
        double (*m_akt)(const double&); //pointer to the aktivation funktion

        hidden(const size_t &connectionC, double(*akt)(const double&))
        : m_connections(connectionC), m_akt(akt){ m_connections.assign(connectionC); }

        hidden(double (*akt)(const double&)) : m_akt(akt){}

        hidden(){}

        void update();

    };

    struct output
    {
        vector<CONNECTION::connection> m_connections;//the connections ending at this neuron
        double (*m_akt)(const double&);

        output(const size_t &connectionC, double (*akt)(const double&))
        : m_connections(connectionC), m_akt(akt){ m_connections.assign(connectionC); }

        output(double (*akt)(const double&)) : m_akt(akt){}

        output(){}

        void update(double &output);
    };

}
