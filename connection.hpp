#ifndef DEFAULT_WEIGHT
#define UNDEFINE_WEIGHT
#define DEFAULT_WEIGHT 1//add macro for random currently 1 for testing purpose
#endif

namespace nnet::CONNECTION{    

    struct connection
    {
        double m_weight;
        double* m_input;

        connection() : m_weight(DEFAULT_WEIGHT){}

        double update(){ return (*m_input) * m_weight; }
        void remap(void* src, void* dest){ m_input = (double*)((char*)m_input + ((char*)dest - (char*)src)); }
    };

}

#ifdef UNDEFINE_WEIGHT
#undef UNDEFINE_WEIGHT
#undef DEFAULT_WEIGHT
#endif