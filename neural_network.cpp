#include "neural_network.hpp"

nnet::neural_network::neural_network(const size_t &input_neurons, const size_t &hidden_neurons, const size_t &output_neurons, double (*akt)(const double&))
{
    m_output.assign<size_t, double(*)(const double&)>(output_neurons, hidden_neurons, akt);
    m_hidden.assign<size_t, double(*)(const double&)>(hidden_neurons, input_neurons, akt);
    m_input.assign<double(*)(const double&)>(input_neurons, akt);
    
    for (auto i = m_output.begin(); i < m_output.end(); i++)
        for (size_t i1 = 0; i1 < hidden_neurons; i1++)
            i->m_connections[i1].m_input = &(m_hidden[i1].m_output);

    for (auto i = m_hidden.begin(); i < m_hidden.end(); i++)
        for (size_t i1 = 0; i1 < input_neurons; i1++)
            i->m_connections[i1].m_input = &(m_input[i1].m_output);

    //std::cout << "finished construction" << std::endl;
}

nnet::neural_network::neural_network(const size_t &input_neurons, const size_t &output_neurons, double (*akt)(const double&))
{
    m_output.assign(output_neurons, input_neurons, akt);
    m_input.assign(input_neurons, akt);

    for (auto i = m_output.begin(); i < m_output.end(); i++)
        for (size_t i1 = 0; i1 < input_neurons; i1++)
            i->m_connections[i1].m_input = &(m_input[i1].m_output);
}

nnet::neural_network::neural_network(const nnet::neural_network_info &constructionInfo)
{

    m_output.assign(constructionInfo.outputLayer.size());
    m_hidden.assign(constructionInfo.hiddenLayer.size());
    m_input .assign(constructionInfo.inputLayer .size());

    auto i01 = constructionInfo.outputLayer.begin();
    vector<nnet::connection_info>::const_iterator i11;

    for (auto i = m_output.begin(); i < m_output.end(); i++)
    {
        i->m_connections.assign(i01->connections.size());
        i->m_akt = i01->aktivationFunction;

        i11 = i01->connections.begin();
        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
        {
            switch (i11->startLayer)
            {
            case hidden:
                i1->m_input = &(m_hidden[i11->startNeuron].m_output);
                break;
            case input:
                i1->m_input = &(m_input[i11->startNeuron].m_output);
                break;
            }

            i11++;
        }

        i01++;
    }

    i01 = constructionInfo.hiddenLayer.begin();

    for (auto i = m_hidden.begin(); i < m_hidden.end(); i++)
    {
        i->m_connections.assign(i01->connections.size());
        i->m_akt = i01->aktivationFunction;

        i11 = i01->connections.begin();
        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
        {
            switch (i11->startLayer)
            {
            case hidden:
                i1->m_input = &(m_hidden[i11->startNeuron].m_output);
                break;
            case input:
                i1->m_input = &(m_input[i11->startNeuron].m_output);
                break;
            }

            i11++;
        }

        i01++;
    }

    for (size_t i = 0; i < m_input.size(); i++)
        m_input[i].m_akt = constructionInfo.inputLayer[i].aktivationFunction;    
}

nnet::neural_network::neural_network(nnet::neural_network &&other)
{
    m_input = std::move(other.m_input);
    m_hidden = std::move(other.m_hidden);
    m_output = std::move(other.m_output);
}

nnet::neural_network_info nnet::neural_network::get_info()
{
    nnet::neural_network_info retVal;

    retVal.outputLayer.assign(m_output.size());
    retVal.hiddenLayer.assign(m_hidden.size());
    retVal.inputLayer .assign(m_input .size());

    vector<nnet::connection_info>::normal_iterator i11;
    auto i01 = retVal.outputLayer.begin();
        

    for (auto i = m_output.begin(); i < m_output.end(); i++)
    {
        i11 = i01->connections.begin();

        i01->aktivationFunction = i->m_akt;
        i01->connections.assign(i->m_connections.size());

        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
        {

            if ((void*)i1->m_input > (void*)&m_hidden && (void*)i1->m_input < (void*)&m_hidden.end()){
                i11->startLayer = hidden;
                i11->startNeuron = get_positionIndex((byte*)(m_hidden.get_ValPtr()), (byte*)(i1->m_input), sizeof(nnet::NEURON::hidden));//calculate neuron position with pointers
            }else{
                i11->startLayer = input;
                i11->startNeuron = get_positionIndex((byte*)(m_input.get_ValPtr()), (byte*)(i1->m_input), sizeof(nnet::NEURON::input));//calculate neuron position with pointers
            }
            
            i11->weight = i1->m_weight;

            i11++;
        }

        i01++;
    }

    i01 = retVal.hiddenLayer.begin();
    
    for (auto i = m_hidden.begin(); i < m_hidden.end(); i++)
    {
        i11 = i01->connections.begin();

        i01->aktivationFunction = i->m_akt;
        i01->connections.assign(i->m_connections.size());

        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
        {
            if ((void*)i1->m_input > (void*)&m_hidden && (void*)i1->m_input < (void*)&m_hidden.end()){
                i11->startLayer = hidden;
                i11->startNeuron = get_positionIndex((byte*)(m_hidden.get_ValPtr()), (byte*)(i1->m_input), sizeof(nnet::NEURON::hidden));//calculate neuron position with pointers
            }else{
                i11->startLayer = input;
                i11->startNeuron = get_positionIndex((byte*)(m_input.get_ValPtr()), (byte*)(i1->m_input), sizeof(nnet::NEURON::input));//calculate neuron position with pointers
            }

            i11->weight = i1->m_weight;

            i11++;
        }

        i01++;
    }

    for (size_t i = 0; i < m_input.size(); i++)
        retVal.inputLayer[i].aktivationFunction = m_input[i].m_akt;

    return retVal;
}

void nnet::neural_network::run(double* output_location, double* input_location)
{
    for (size_t i = 0; i < m_input.size(); i++)
        m_input[i].update(input_location[i]);
    //std::cout << "input run succesfull" << std::endl;
    for (auto i = m_hidden.begin(); i < m_hidden.end(); i++)
        i->update();
    //std::cout << "hidden run succesfull" << std::endl;
    for (auto i = m_output.begin(); i < m_output.end(); i++)
        i->update(*output_location);
    //std::cout << "output run succesfull" << std::endl;
}

void nnet::neural_network::remap_hidden(void* prevVectorPos)
{
    for(auto i = m_output.begin(); i < m_output.end(); i++)
        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
            if((void*)&(m_hidden.end()) > i1->m_input && i1->m_input > (void*)&(m_hidden.end()))
                i1->remap(prevVectorPos, m_hidden.get_ValPtr());

    for(auto i = m_hidden.begin(); i < m_hidden.end(); i++)
        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
            if((void*)&(m_hidden.end()) > i1->m_input && i1->m_input > (void*)&(m_hidden.end()))
                i1->remap(prevVectorPos, m_hidden.get_ValPtr());
}

void nnet::neural_network::remap_input(void* prevVectorPos)
{
    for(auto i = m_output.begin(); i < m_output.end(); i++)
        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
            if((void*)&(m_input.end()) > i1->m_input && i1->m_input > (void*)&(m_input.end()))
                i1->remap(prevVectorPos, m_input.get_ValPtr());

    for(auto i = m_hidden.begin(); i < m_hidden.end(); i++)
        for (auto i1 = i->m_connections.begin(); i1 < i->m_connections.end(); i1++)
            if((void*)&(m_input.end()) > i1->m_input && i1->m_input > (void*)&(m_input.end()))
                i1->remap(prevVectorPos, m_input.get_ValPtr());
}