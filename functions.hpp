#include "datatypes.hpp"

size_t get_positionIndex(byte* ArrayStart, byte* object, const size_t &objectSize){ return (object - ArrayStart) / objectSize; }